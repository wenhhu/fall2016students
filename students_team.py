from __future__ import division, print_function, absolute_import
import pandas as pd
import random

DATA_FILE = "students.csv"


def main():
    """
    Main entry point to the program

    :return:
    """

    # Set seed
    random.seed(100)

    # read data file

    df_students = pd.read_csv(DATA_FILE)
    # Add Full name column
    df_students['Full'] = df_students.Last.str.cat(df_students.First, sep=", ")

    # Create a list of full names
    names = df_students.Full.copy().values

    # randomize the names
    random.shuffle(names)

    # Print the teams
    for i in range(0, len(names), 2):
        if i+1 == len(names):
            print(names[i])
        else:
            print ("{} / {}".format(names[i], names[i+1]))


if __name__ == "__main__":
    main()